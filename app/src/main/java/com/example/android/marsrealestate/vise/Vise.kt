package com.example.android.marsrealestate.vise

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Vise(
        val id: String,
        val model: String,
        @Json(name = "brand") val producer: String,
        val typeOfVice: String,
        @Json(name = "photo") val img: String) : Parcelable {

}