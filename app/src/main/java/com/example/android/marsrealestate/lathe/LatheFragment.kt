package com.example.android.marsrealestate.lathe

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController

import com.akordirect.vmccnc.databinding.FragmentLatheBinding


class LatheFragment: Fragment() {

    private val viewModel: LatheViewModel by lazy {
        ViewModelProviders.of(this).get(LatheViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = FragmentLatheBinding.inflate(inflater)
//        val binding = GridViewLatheBinding.inflate(inflater)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel


        binding.lathesGrid.adapter = LathePhotoGridAdapter(LathePhotoGridAdapter.OnClickListenerLathe{
            viewModel.displayOneLathe(it)
            Log.d("LatheFragment dm->", "element is clicked")
        })

        viewModel.navigateToSelectedLathe.observe(this, Observer {
            if ( null != it ) {
                this.findNavController().navigate(
                        LatheFragmentDirections.actionLatheFragmentToLatheOneFragment(it))
                viewModel.displayOneLatheComplete()
            }
        })

//        setHasOptionsMenu(true)
        return binding.root
    }


//    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        inflater.inflate(R.menu.Lathe_menu, menu)
//        super.onCreateOptionsMenu(menu, inflater)
//    }

//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        viewModel.updateFilter(
//                when (item.itemId) {
//                    R.id.show_3_axes_menu -> LatheApiFilter.AXES_3
//                    R.id.show_3_plus_2_axes_menu -> LatheApiFilter.AXES_3_PLUS_2
//                    R.id.show_4_axes_menu -> LatheApiFilter.AXES_4
//                    R.id.show_5_axes_menu-> LatheApiFilter.AXES_5
//                    else -> LatheApiFilter.AXES_ALL
//                }
//        )
//        return true
//    }
}