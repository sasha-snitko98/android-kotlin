package com.example.android.marsrealestate.vmc

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.GridViewVmcBinding

class VmcPhotoGridAdapter(private val onClickListenerVmc: OnClickListenerVmc ) : ListAdapter<VmcProperty, VmcPhotoGridAdapter.VmcPropertyviewHolder>(DiffCallback) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VmcPropertyviewHolder {
        return VmcPropertyviewHolder(GridViewVmcBinding.inflate(LayoutInflater.from(parent.context)))
    }


    override fun onBindViewHolder(holder: VmcPropertyviewHolder, position: Int) {
        val vmcProperty = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerVmc.onClick(vmcProperty)
        }
        holder.bind(vmcProperty)
    }


    companion object DiffCallback : DiffUtil.ItemCallback<VmcProperty>() {
        override fun areItemsTheSame(oldItem: VmcProperty, newItem: VmcProperty): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: VmcProperty, newItem: VmcProperty): Boolean {
            return oldItem.id == newItem.id
        }
    }


    class VmcPropertyviewHolder(private var binding: GridViewVmcBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(vmcProperty: VmcProperty) {
            binding.property = vmcProperty
            binding.executePendingBindings()
        }

    }

    class OnClickListenerVmc(val clickListener: (vmcProperty: VmcProperty) -> Unit) {
        fun onClick(vmcProperty: VmcProperty) = clickListener(vmcProperty)
    }

}