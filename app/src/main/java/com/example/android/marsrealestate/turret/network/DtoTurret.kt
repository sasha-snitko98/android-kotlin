/*
 * Copyright (C) 2019 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.marsrealestate.turret.network


import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class NetworkTurretContant(val content: List<NetworkTurret>)



@JsonClass(generateAdapter = true)
data class NetworkTurret(
        val model: String,
        val manufacturer: String,
        val url: String,
        val vdi: String,
        val video: String,
        val exist: String?)


//fun NetworkTurretContant.asDatabaseModel(): List<DatabaseVideo> {
//    return content.map {
//        DatabaseVideo(
//                title = it.model,
//                description = it.manufacturer,
//                url = it.url,
//                updated = it.vdi,
//                thumbnail = it.video)
//    }
//}

