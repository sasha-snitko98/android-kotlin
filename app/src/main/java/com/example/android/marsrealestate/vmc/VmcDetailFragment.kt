package com.example.android.marsrealestate.vmc

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.akordirect.vmccnc.databinding.FragmentDetailVmcBinding

class VmcDetailFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val application = requireNotNull(activity).application
        val binding =  FragmentDetailVmcBinding.inflate(inflater)
        binding.lifecycleOwner = this

        val vmcProperty = VmcDetailFragmentArgs.fromBundle(arguments!!).selectedVmcProperty
        val viewModelFactory = VmcDetailViewModelFactory(vmcProperty, application)
        binding.viewModel = ViewModelProviders.of(
                this, viewModelFactory).get(VmcDetailViewModel::class.java)

         return binding.root
    }

}